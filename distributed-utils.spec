%define debug_package %{nil}
%global oh_version OpenHarmony-v3.2-Release

%global c_utils_dir %{_builddir}/commonlibrary/c_utils
%global config_policy_dir %{_builddir}/base/customization/config_policy
%global dsoftbus_dir %{_builddir}/foundation/communication/dsoftbus
%global eventhandler_dir %{_builddir}/base/notification/eventhandler
%global init_dir %{_builddir}/base/startup/init
%global ipc_dir %{_builddir}/foundation/communication/ipc
%global safwk_dir %{_builddir}/foundation/systemabilitymgr/safwk
%global samgr_dir %{_builddir}/foundation/systemabilitymgr/samgr
%global sqlite_dir %{_builddir}/third_party/sqlite
%global libcoap_dir %{_builddir}/third_party/libcoap
%global huks_dir %{_builddir}/base/security/huks
%global device_auth_dir %{_builddir}/base/security/device_auth
%global mbedtls_dir %{_builddir}/third_party/mbedtls
%global device_manager_dir %{_builddir}/foundation/distributedhardware/device_manager
%global json_dir %{_builddir}/third_party/json
%global dataclassification_dir %{_builddir}/base/security/dataclassification
%global device_security_level_dir %{_builddir}/base/security/device_security_level

%global build_opt /opt/distributed-middleware-build

Name:       distributed-utils
Version:    1.0.0
Release:    2
Summary:    Distributed middleware used components.
License:    Apache-2.0
Url:        https://gitee.com/openharmony/
Source0:  https://gitee.com/openharmony/commonlibrary_c_utils/repository/archive/openHarmony-v3.2-Release.tar.gz #/commonlibrary_c_utils-OpenHarmony-v3.2-Release.tar.gz
Source1:  https://gitee.com/openharmony/customization_config_policy/repository/archive/openHarmony-v3.2-Release.tar.gz #/customization_config_policy-OpenHarmony-v3.2-Release.tar.gz
Source2:  https://gitee.com/openharmony/communication_dsoftbus/repository/archive/openHarmony-v3.2-Release.tar.gz #/communication_dsoftbus-OpenHarmony-v3.2-Release.tar.gz
Source3:  https://gitee.com/openharmony/notification_eventhandler/repository/archive/openHarmony-v3.2-Release.tar.gz #/notification_eventhandler-OpenHarmony-v3.2-Release.tar.gz
Source4:  https://gitee.com/openeuler/distributed-beget/repository/archive/distributed-beget.tar.gz
Source5:  https://gitee.com/openharmony/communication_ipc/repository/archive/openHarmony-v3.2-Release.tar.gz #/communication_ipc-OpenHarmony-v3.2-Release.tar.gz
Source6:  https://gitee.com/openharmony/systemabilitymgr_safwk/repository/archive/openHarmony-v3.2-Release.tar.gz #/systemabilitymgr_safwk-OpenHarmony-v3.2-Release.tar.gz
Source7:  https://gitee.com/openharmony/systemabilitymgr_samgr/repository/archive/openHarmony-v3.2-Release.tar.gz #/systemabilitymgr_samgr-OpenHarmony-v3.2-Release.tar.gz
Source8:  https://gitee.com/openharmony/third_party_sqlite/repository/archive/openHarmony-v3.2-Release.tar.gz #/third_party_sqlite-OpenHarmony-v3.2-Release.tar.gz
Source9:  https://gitee.com/openharmony/third_party_libcoap/repository/archive/openHarmony-v3.2-Release.tar.gz #/third_party_libcoap-OpenHarmony-v3.2-Release.tar.gz
Source10: https://gitee.com/openharmony/security_huks/repository/archive/openHarmony-v3.2-Release.tar.gz #/security_huks-OpenHarmony-v3.2-Release.tar.gz
Source11: https://gitee.com/openharmony/security_device_auth/repository/archive/openHarmony-v3.2-Release.tar.gz #/security_device_auth-OpenHarmony-v3.2-Release.tar.gz
Source12: https://gitee.com/openharmony/third_party_mbedtls/repository/archive/openHarmony-v3.2-Release.tar.gz #/third_party_mbedtls-OpenHarmony-v3.2-Release.tar.gz
Source13: https://gitee.com/openharmony/distributedhardware_device_manager/repository/archive/openHarmony-v3.2-Release.tar.gz #/distributedhardware_device_manager-OpenHarmony-v3.2-Release.tar.gz
Source14: https://gitee.com/openharmony/third_party_json/repository/archive/openHarmony-v3.2-Release.tar.gz #/third_party_json-OpenHarmony-v3.2-Release.tar.gz
Source15: https://gitee.com/openharmony/security_dataclassification/repository/archive/openHarmony-v3.2-Release.tar.gz #/security_dataclassification-OpenHarmony-v3.2-Release.tar.gz
Source16: https://gitee.com/openharmony/security_device_security_level/repository/archive/openHarmony-v3.2-Release.tar.gz #/security_device_security_level-OpenHarmony-v3.2-Release.tar.gz

Patch0: 0000-adapt-for-build-c_utils.patch
Patch1: 0001-remove-dependency-and-adpat-for-build-config_policy.patch
Patch2: 0002-remove-dependency-and-adapt-for-openeuler-dsoftbus.patch
Patch3: 0003-remove-dependency-and-adapt-for-build-eventhandler.patch
Patch4: 0004-remove-dependency-and-adapt-for-build-ipc.patch
Patch5: 0005-remove-dependency-on-hitrace-safwk.patch
Patch6: 0006-remove-dependency-and-adapt-for-build-samgr.patch
Patch7: 0007-adapt-for-build-sqlite.patch
Patch8: 0008-remove-dependency-and-adapt-for-build-huks.patch
Patch9: 0009-remove-dependency-and-adapt-for-build-device_auth.patch
Patch10: 0010-adapt-for-build-mbedtls.patch
Patch11: 0011-remove-dependency-and-adapt-for-build-device_manager.patch
Patch12: 0012-remove-dependency-and-adapt-for-build-device_security_level.patch

BuildRequires: python3-jinja2 python3-pyyaml cjson cjson-devel
BuildRequires: libatomic libicu-devel libxml2-devel compat-openssl11-devel
BuildRequires: distributed-build hilog

%description
Tools used by distributed middleware.

# Generate directory structure for build.
%prep
rm -rf %{_builddir}/*

cp -rp %{build_opt} %{_builddir}/build
[ ! -L "%{_builddir}/build.sh" ] && ln -s %{_builddir}/build/build_scripts/build.sh %{_builddir}/build.sh
[ ! -L "%{_builddir}/.gn" ] && ln -s %{_builddir}/build/core/gn/dotfile.gn %{_builddir}/.gn
[ ! -L "%{_builddir}/build.py" ] && ln -s %{_builddir}/build/lite/build.py %{_builddir}/build.py
cp -r %{_builddir}/build/openeuler/vendor %{_builddir}/
cp -r %{_builddir}/build/openeuler/compiler_gn/third_party %{_builddir}/
cp -rf %{_builddir}/build/openeuler/compiler_gn/base %{_builddir}/

rm -rf %{_builddir}/third_party/mbedtls/*

%setup -q -D -T -a 0 -c -n %{c_utils_dir}/..
%setup -q -D -T -a 1 -c -n %{config_policy_dir}/..
%setup -q -D -T -a 2 -c -n %{dsoftbus_dir}/..
%setup -q -D -T -a 3 -c -n %{eventhandler_dir}/..
%setup -q -D -T -a 4 -c -n %{init_dir}/..
%setup -q -D -T -a 5 -c -n %{ipc_dir}/..
%setup -q -D -T -a 6 -c -n %{safwk_dir}/..
%setup -q -D -T -a 7 -c -n %{samgr_dir}/..
%setup -q -D -T -a 8 -c -n %{sqlite_dir}/..
%setup -q -D -T -a 9 -c -n %{libcoap_dir}/..
%setup -q -D -T -a 10 -c -n %{huks_dir}/..
%setup -q -D -T -a 11 -c -n %{device_auth_dir}/..
%setup -q -D -T -a 12 -c -n %{mbedtls_dir}/..
%setup -q -D -T -a 13 -c -n %{device_manager_dir}/..
%setup -q -D -T -a 14 -c -n %{json_dir}/..
%setup -q -D -T -a 15 -c -n %{dataclassification_dir}/..
%setup -q -D -T -a 16 -c -n %{device_security_level_dir}/..

%patch0 -p1 -d %{c_utils_dir}
%patch1 -p1 -d %{config_policy_dir}
%patch2 -p1 -d %{dsoftbus_dir}
%patch3 -p1 -d %{eventhandler_dir}
%patch4 -p1 -d %{ipc_dir}
%patch5 -p1 -d %{safwk_dir}
%patch6 -p1 -d %{samgr_dir}
%patch7 -p1 -d %{sqlite_dir}
%patch8 -p1 -d %{huks_dir}
%patch9 -p1 -d %{device_auth_dir}
%patch10 -p1 -d %{mbedtls_dir}
%patch11 -p1 -d %{device_manager_dir}
%patch12 -p1 -d %{device_security_level_dir}

# build all components with build.sh
%build
cd %{_builddir}
rm -rf %{_builddir}/out

%ifarch x86_64
./build.sh --product-name openeuler --target-cpu x86_64
%endif

%ifarch aarch64
./build.sh --product-name openeuler --target-cpu arm64
%endif

%install
install -d -m 0755 %{buildroot}/system/lib64
install -d -m 0755 %{buildroot}/system/bin
install -d -m 0755 %{buildroot}/usr/lib64
install -d -m 0755 %{buildroot}/usr/bin
install -d -m 0755 %{buildroot}%{_includedir}/c_utils
install -d -m 0755 %{buildroot}%{_includedir}/config_policy
install -d -m 0755 %{buildroot}%{_includedir}/dsoftbus
install -d -m 0755 %{buildroot}%{_includedir}/eventhandler
install -d -m 0755 %{buildroot}%{_includedir}/init
install -d -m 0755 %{buildroot}%{_includedir}/ipc
install -d -m 0755 %{buildroot}%{_includedir}/safwk
install -d -m 0755 %{buildroot}%{_includedir}/samgr
install -d -m 0755 %{buildroot}%{_includedir}/huks
install -d -m 0755 %{buildroot}%{_includedir}/device_auth
install -d -m 0755 %{buildroot}%{_includedir}/device_manager
install -d -m 0755 %{buildroot}%{_includedir}/nlohmann_json
install -d -m 0755 %{buildroot}%{_includedir}/nlohmann_json/single_include
install -d -m 0755 %{buildroot}%{_includedir}/dataclassification
install -d -m 0755 %{buildroot}%{_includedir}/device_security_level

%ifarch aarch64
module_out_path="out/openeuler/linux_clang_arm64"
header_out_path="out/openeuler/innerkits/linux-arm64"
%endif
%ifarch x86_64
module_out_path="out/openeuler/linux_clang_x86_64"
header_out_path="out/openeuler/innerkits/linux-x86_64"
%endif

# copy shared libs and static libs.
pushd %{_builddir}/${module_out_path}
for so_file in $(find . -name *.z.so -type f)
do
  cp ${so_file} %{buildroot}/system/lib64
  cp ${so_file} %{buildroot}/usr/lib64
done

for a_file in $(find ./ -name *.a -type f)
do
  cp ${a_file} %{buildroot}/system/lib64
  cp ${so_file} %{buildroot}/usr/lib64
done
popd

# copy executable file.
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/sa_main %{buildroot}/system/bin/
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/samgr %{buildroot}/system/bin/
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/deviceauth_service %{buildroot}/system/bin/
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/sa_main %{buildroot}/usr/bin/
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/samgr %{buildroot}/usr/bin/
install -m 755 %{_builddir}/out/openeuler/packages/phone/system/bin/deviceauth_service %{buildroot}/usr/bin/

# copy header files of component to includedir.
pushd %{_builddir}/${header_out_path}
find c_utils -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/c_utils
find config_policy -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/config_policy
find dsoftbus -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/dsoftbus
find eventhandler -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/eventhandler
find init -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/init
find ipc -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/ipc
find safwk -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/safwk
find samgr -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/samgr
find huks -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/huks
find device_auth -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/device_auth
find device_manager -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/device_manager
find dataclassification -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/dataclassification
find device_security_level -name *.h -print0 | xargs -0 -i cp -rvf {} %{buildroot}%{_includedir}/device_security_level
popd

# copy nlohmann_json header file to includedir.
cd %{json_dir}
cp -rvf include/nlohmann %{buildroot}/%{_includedir}/nlohmann_json
cp -rvf single_include/nlohmann %{buildroot}/%{_includedir}/nlohmann_json/single_include

%files
/system/lib64/*
/system/bin/*
/%{_includedir}/*
/usr/lib64/*
/usr/bin/*

%changelog
* Wed Jun 21 2023 Ge Wang <wang__ge@126.com> - 1.0.0-2
- replace buildrequire from openssl to compat-openssl11

* Tue Jun 20 2023 hepeng <hepeng68@huawei.com> - 1.0.0-1
- add components used by distributed middleware.
